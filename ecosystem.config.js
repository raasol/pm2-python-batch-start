module.exports = {
  apps : [{
    name: 'one',
    script: 'C:\\Users\\administrator.DAREX\\Desktop\\pm2-python-batch-start\\one.py',
    args: 'arg1',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    interpreter: "python"
  },
  {
    name: 'two',
    script: 'C:\\Users\\administrator.DAREX\\Desktop\\pm2-python-batch-start\\one.py',
    args: 'arg2',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    interpreter: "python"
  },
  {
    name: 'partner',
    script: 'C:\\Program Files\\WoodNET_API\\import_business_partners.py',
    args: 'arg2',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    interpreter: "python"
  },
  {
    name: 'wflogs',
    script: 'C:\\Program Files\\PythonScripts\\transfer_wf_log_automatic.py',
    args: 'arg2',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    interpreter: "python"
  },
  {
    name: 'wnapi',
    script: 'C:\\Program Files\\WoodNET_API\\wood_net_api.py',
    args: 'arg2',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    interpreter: "python"
  }],

  deploy : {}
};
